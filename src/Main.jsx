import React, { Component } from 'react';
import LoginPage from './LoginPage'
import Home from './Home'
import Req from './data/req.js';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {is_logged_in: Req.isLoggedIn()};
    }

    getView() {
        if(this.state.is_logged_in) {
            return <Home/>
        } else {
            return <LoginPage />
        }
    }

    render() {
        return (
            <div>{this.getView()}</div>
        )
    }

}



export default Main;