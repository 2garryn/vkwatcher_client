import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';

class ErrorView extends React.Component {
    render() {
        return (
            <div> 
                Что-то пошло не так. <Button onClick={this.props.retryClb}>Повторить</Button>
            </div>
        )
    }

}


export default ErrorView;