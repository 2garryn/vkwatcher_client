import deepcopy from 'deepcopy';

export function alyticsMgmt(state = {}, action) {
    switch (action.type) {
        /*
        case 'FETCH_ALYTICS_LOADING': {
            let st = deepcopy(state)
            if (st[action.group_id]) {
                st[action.group_id].loading = action.bool
            } else {
                st[action.group_id] = {loading: action.bool}
            }
            return st
        } 

        case 'FETCH_ALYTICS_SUCCESS': {
            let st = deepcopy(state)
            st[action.group_id].events = action.data
            st[action.group_id].loaded = true
            return st
        }
                */
        case 'FETCH_ALYTICS_COUNTERS_LOADING': {
            let st = deepcopy(state)
            st.counters_loading = action.bool
            st.current_groupid = action.groupId
            return st
        }
        case 'FETCH_ALYTICS_COUNTERS_SUCCESS': {
            let st = deepcopy(state)
            st.counters_list = action.countersList 
            st.start_date = action.startDate
            st.end_date = action.endDate
            st.current_groupid = action.groupId
            return st
        }
        case 'RESET_ALYTICS_COUNTERS': {
            let st = deepcopy(state)
            st.counters_loading = false
            st.current_groupid = undefined
            st.counters_list = undefined 
            st.start_date = undefined
            st.end_date = undefined
            return st
        }



        default:
            return state;
    }
}