import { combineReducers } from 'redux';
import { navigation } from './navigation';
import { groupsMgmt } from './groupsMgmt';
import { advListMgmt } from './advListMgmt';
import { alyticsMgmt } from './alyticsMgmt';
import { errorMgmt } from './errorMgmt'
export default combineReducers({
    groupsMgmt,
    advListMgmt,
    alyticsMgmt,
    errorMgmt,
    navigation
});