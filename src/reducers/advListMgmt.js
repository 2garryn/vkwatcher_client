import deepcopy from 'deepcopy';
import _ from 'underscore';

export function advListMgmt(state = {}, action) {
    switch (action.type) {
        case 'FETCH_ADVLISTS_LOADING': {
            let st = deepcopy(state)
            if (st.listAdvLists[action.group_id]) {
                st.listAdvLists[action.group_id].loading = action.bool
            } else {
                st.listAdvLists[action.group_id] = {loading: action.bool}
            }
            if (action.bool) {
                st.listAdvLists[action.group_id].error = false
            }
            return st
        }
        case 'FETCH_ADVLISTS_SUCCESS': {
            let st = deepcopy(state)
            st.listAdvLists[action.group_id].list = action.lists
            return st
        }
        case 'FETCH_ADVLISTS_ERROR': {
            let st = deepcopy(state)
            st.listAdvLists[action.group_id].error = true
            return st
        }


        ///
        case 'ADD_ADVLIST_LOADING':
            let st = deepcopy(state)
            if (st.listAdvLists[action.group_id]) {
                st.listAdvLists[action.group_id].create_loading = action.bool
            } else {
                st.listAdvLists[action.group_id] = {create_loading: action.bool}
            }
            return st
        case 'ADD_ADVLIST_SUCCESS': {
            let st = deepcopy(state)
            st.listAdvLists[action.group_id].list.unshift(action.data)
            return st
        }


        case 'ADV_LIST_UPLOADING_START': {
            console.log(action)
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.map((item) => {
                if (item.id == action.list_id) {
                   item.file_uploading = true
                   return item
                } else {
                    return item
                }
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }
        case 'ADV_LIST_UPLOADING_DONE': {
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.map((item) => {
                if (item.id == action.list_id) {
                   item.file_uploading = false
                   item.users_uploaded_count = action.users_count,
                   item.file_uploaded = true
                   return item
                } else {
                    return item
                }
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }

        case 'ADV_LIST_DELETE_UPLOADED_LOADING': {
            console.log(action)
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.map((item) => {
                if (item.id == action.list_id) {
                   item.file_deleting = true
                   return item
                } else {
                    return item
                }
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }

        case 'ADV_LIST_DELETE_UPLOADED_DONE': {
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.map((item) => {
                if (item.id == action.list_id) {
                   item.file_deleting = false
                   item.users_uploaded_count = 0,
                   item.file_uploaded = false
                   return item
                } else {
                    return item
                }
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }

        case 'ADV_LIST_DELETE_LOADING': {
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.map((item) => {
                if (item.id == action.list_id) {
                    item.deleting = true
                    return item
                } else {
                    return item
                }
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }
        case 'ADV_LIST_DELETE_SUCCESS': {
            let st = deepcopy(state)
            let newList = st.listAdvLists[action.group_id].list.filter((item) => {
                console.log({"ITEMS HERE": item, "ACTION": action})
                return item.id !== action.list_id
            })
            st.listAdvLists[action.group_id].list = newList
            return st
        }
        

        default:
            return state;
    }
}