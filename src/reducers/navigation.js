

export function navigation(state = false, action) {
    switch (action.type) {
        case 'SET_MAIN_SCREEN':
            return {label: action.label, page: action.page};
        default:
            return state;
    }
}