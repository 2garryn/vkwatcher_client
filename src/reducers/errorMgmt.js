import deepcopy from 'deepcopy';

export function errorMgmt(state = {}, action) {
    switch (action.type) {
        case 'SHOW_INTERNAL_ERROR': {
            let st = deepcopy(state)
            st['show'] = true
            st['retryClb'] = action.retryCallback
            st['cancelClb'] = action.cancelCallback
            return st
        }
        case 'HIDE_ERROR': {
            return {}
        }
    
        default:
            return state
    }

}