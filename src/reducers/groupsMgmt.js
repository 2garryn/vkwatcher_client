import deepcopy from 'deepcopy';

var _ = require('underscore');


const itemsOnPage = 15


export function groupsMgmt(state = {}, action) {
    switch (action.type) {
        case 'ADD_GROUP_LOADING':
            return Object.assign({}, state, {
                addGroupLoading: action.bool
            })
        case 'ADD_GROUP_SUCCESS':
            let ng = state.listGroups.slice(0, state.listGroups.length)
            ng.unshift(action.group)
            return Object.assign({}, state, {
                listGroups: ng
            })
        case 'FETCH_LIST_GROUPS_LOADING':
            return Object.assign({}, state, {
                listGroupsLoading: action.bool
            })
        case 'FETCH_LIST_GROUPS_SUCCESS':
            return Object.assign({}, state, {
                listGroups: action.groups
            })
        case 'LIST_EVENTS_LOADING': {
                let event = state.events[action.eventType] ? state.events[action.eventType] : {}
                let newEvent = {
                    loading: action.bool
                }
                if(action.bool) {
                    newEvent.error = false
                }
                if(!action.reload) {
                    newEvent = Object.assign({}, event, newEvent) 
                }
                let needed = {}
                needed[action.eventType] = newEvent
                return Object.assign({}, state, {
                    events: Object.assign({}, state.events, needed)
                })
        }
        case 'LIST_EVENTS_ERROR': {
            let st = deepcopy(state)
            let event = st.events[action.eventType] ? st.events[action.eventType] : {}
            event.error = true
            st.events[action.eventType] = event
            return st
        }
        case 'LIST_EVENTS_RECEIVED_REQ_ID': {
            let newEvent = {
                request_id: action.request_id,
                count: action.count
            }
            let items = []
            for(let i = 0; i < action.count; i++) {
                items.push({received: false, key: i})
            }
            newEvent.list = items
            let needed = {}
            needed[action.eventType] = newEvent
            return Object.assign({}, state, {
                events: Object.assign({}, state.events, needed)
            })
        }
        case 'LIST_EVENTS_SUCCESS' : {
            let event = state.events[action.eventType] ? state.events[action.eventType] : {}
            let realLimit = action.limit > action.events.length ? action.events.length : action.limit
            let last = action.offset + realLimit
            let copied = _.map(event.list, _.clone);
            for(let i = action.offset; i < last; i++ ) {
                let key = copied[i].key
                copied[i] = action.events[i - action.offset]
                copied[i].key = key
                copied[i].received = true
            }
            let newEvent = Object.assign({}, event, {
                list: copied
            })
            let needed = {}
            needed[action.eventType] = newEvent
            return Object.assign({}, state, {
                events: Object.assign({}, state.events, needed)
            })
        }
        default:
            return state;
    }
}