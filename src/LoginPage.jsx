import React from 'react';
import { Form, Icon, Input, Button, message, Layout, Spin } from 'antd';
import Req from './data/req.js';
import './css/loginPage.css'
const Header = Layout.Header
const Footer = Layout.Footer
const Content = Layout.Content
const FormItem = Form.Item


class LoginPageNested extends React.Component {

    state = {
        loading: false
    }

    sendLoginRequest(username, password) {
        let hide = message.loading('Логинимся...');
        this.setState({loading: true}, () => {
            Req.login(username, password).then((reply) => {
                hide()
                if(reply.code == 1) {
                    message.error('Сервер временно недоступен');
                } else if (reply.code == 20001 || reply.code == 10001) {
                    message.error('Неправильное имя пользователя или пароль');
                }
                this.setState({loading: false})
            })
        })
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
             this.sendLoginRequest(values.username, values.password)
          }
        });
      }

    render() {
        const { getFieldDecorator } = this.props.form;
   /*     if (this.state.loading) {
            return <Spin size="large" />
        }
*/
        return (
            <Form onSubmit={this.handleSubmit} className='loginForm'>
                <FormItem>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Введите имя пользователя!' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Имя пользователя" disabled={this.state.loading} /> 
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Введите пароль!' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Пароль" disabled={this.state.loading}/>
                    )}
                </FormItem>
                <FormItem> 
                    <Button type="primary" htmlType="submit" style={{'width': '100%'}} disabled={this.state.loading}>
                        Войти
                    </Button>
                </FormItem>
            </Form>
        )
    }
}
class LoginPage extends React.Component {

    render() {
        const Blabla = Form.create()(LoginPageNested);
        return (
            <div className='loginDiv'>
                    <Blabla />
            </div>
        )
    }
}



export default LoginPage