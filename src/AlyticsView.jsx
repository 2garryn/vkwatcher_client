import React from 'react';
import { DatePicker, Checkbox, Row, Col, Select, Spin, Button } from 'antd';
import AlyticsViewTable from './AlyticsViewTable';
import { resetAlyticsCounters } from './actions/alyticsMgmt';
import { connect } from 'react-redux';

class AlyticsView extends React.Component {
    state = {
        startDate: null,
        endDate: null,
        startDateString: null,
        endDateString: null,
        infoTab: 'table'
    }
    constructor(props) {
        super(props);

    }
    componentDidMount() {
 
    }
    onChangeStartDate(startDate, startDateString) {
        this.setState({startDate, startDateString}, () => {
    //        this.loadAlytics()
        })
    }
    disabledStartDate = (startDate) => {
        const endDate = this.state.endDate;
        if (!startDate || !endDate) {
          return false;
        }
        return startDate.valueOf() > endDate.valueOf();
    }
    onChangeEndDate(endDate, endDateString) {
        this.setState({endDate, endDateString}, () => {
       //     this.loadAlytics()
        })
    }
    disabledEndDate = (endDate) => {
        const startDate = this.state.startDate;
        if (!endDate || !startDate) {
          return false;
        }
        return endDate.valueOf() <= startDate.valueOf();
    }
    isDateSelected() {
        console.log({"CURRENT_STATE": this.state})
        return this.state.startDate != null && this.state.endDate != null
    }

    resetButtonClb() {
        this.setState({
            startDate: null, 
            endDate: null,
            startDateString: null,
            endDateString: null
        }, () => {
            this.props.resetCounters()
        })
    }

    renderSelectDateReqs() {
        return <h2> Выберите даты для отображения </h2>
    }

    renderInfoTabs() {
        if(this.state.infoTab == 'table') {
            return <AlyticsViewTable 
                        groupId={this.props.groupId}
                        startDate={this.state.startDateString} 
                        endDate={this.state.endDateString}
                    />
        } else if(this.state.infoTab == 'graph') {
            return 'Graph'
        }
    }

    renderSelectViewButtons() {
        return (
            <div>
                <Button onClick={() => this.setState({infoTab: 'table'})}>Таблица</Button>
                <Button onClick={() => this.setState({infoTab: 'graph'})}>График</Button>
            </div>
        )
    }

    render() {
        return (
            <div>
                <Row align="middle" style={{ marginBottom: 8 }}>
                    <Col span={3}>
                         Дата начала поиска:
                    </Col>
                    <Col span={4}>
                        <DatePicker 
                            onChange={(d, dateString) => this.onChangeStartDate(d, dateString)} 
                            placeholder="Выберите дату" 
                            disabled={this.isDateSelected()} 
                            disabledDate={this.disabledStartDate}
                            value={this.state.startDate}
                        />
                    </Col>
                    <Col span={17}>
                        {this.isDateSelected() ? <Button icon="search" onClick={this.resetButtonClb.bind(this)}>Новый поиск</Button> : ''}
                    </Col>
                </Row>    
                <Row align="middle" style={{ marginBottom: 8 }}>
                        <Col span={3}>
                            Дата конца поиска:
                        </Col>
                        <Col span={21}>
                            <DatePicker 
                                placeholder="Выберите дату" 
                                disabled={this.isDateSelected()} 
                                onChange={(d, dateString) => this.onChangeEndDate(d, dateString)} 
                                disabledDate={this.disabledEndDate}
                                value={this.state.endDate}
                            />
                        </Col>
                </Row>  
                {this.isDateSelected() ? this.renderInfoTabs() : this.renderSelectDateReqs()}
            </div>

        )
    }
}

const mapStateToProps = (state, props) => {
    return {

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        resetCounters: () => dispatch(resetAlyticsCounters()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlyticsView);