
let overrides =  {
  //  "@primary-color": "#1DA57A"
    "@font-size-base" : "12px",
    "@input-height-base": "24px",
    "@btn-height-base" : "24px",
    "@padding-lg": "16px"
}

module.exports = {
    overrides
}