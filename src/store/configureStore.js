import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import logger from 'redux-logger'
export default function configureStore(initialState) {
    initialState['groupsMgmt'] = {
        listGroups: [],
        events: {}
    }
    initialState['advListMgmt'] = {
        listAdvLists: {}
    }
    initialState['errorMgmt'] = {
        errorMgmt: {}
    }
    return createStore(
        rootReducer,
        initialState,
        compose( 
            applyMiddleware(thunk),
            applyMiddleware(logger)
        )
    );
}