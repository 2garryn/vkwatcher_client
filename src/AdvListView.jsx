import React from 'react';
import PropTypes from 'prop-types';
import { List, Avatar, Spin, Button, Modal, Form, Input, Upload, Icon } from 'antd';
import { fetchAdvLists, addAdvList, advUploadingStart, advUploadingDone, deleteAdvListUploaded, deleteAdvList } from './actions/advListMgmt';
import ErrorView from './ErrorView';
import { connect } from 'react-redux';
const { TextArea } = Input;
const FormItem = Form.Item
const confirm = Modal.confirm;
class AdvListView extends React.Component {

    state = {
        name: '',
        description: '',
        showCreate: false,
        showDelete: false,
        deleteItem: {}
    }

    componentDidMount() {
        this.props.loadList(this.props.groupId)
    }
    getSnapshotBeforeUpdate(prevProps, prevState) { 
        if(prevProps.create_loading == true &&  this.props.create_loading == false) {
            this.setState({showCreate: false, name: '', description: ''})
        }
        return null
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    createDialog() {
        let handleOk = () => {
            this.props.addList(this.state.name, this.state.description, this.props.groupId)
        }

        let form =             
            <Form onSubmit={() => {}}>
                <FormItem label="Название списка">
                    <Input onChange={this.handleChange('name')} />
                </FormItem>
                <FormItem label="Описание списка">
                    <TextArea rows={4} onChange={this.handleChange('description')} />
                </FormItem>
            </Form>  
        return (
            <Modal
                title="Создание списка аналитики"
                visible={this.state.showCreate}
                onOk={handleOk}
                onCancel={() => this.setState({showCreate: false, name: '', description: ''})}
            >
            {
                this.props.create_loading ? <Spin size="large" /> : form
            }
            </Modal>
        )
    }
    renderUploadFile(item) {
            let listId = item.id
            const uploadSettings = {
                name: 'file',
                action: 'https://ogarochek.ru/advlist/upload',
                data: {
                  advlist_id: listId,
                },
                onChange: (event) => {
                    let status = event.file.status
                    let response = event.file.response
                    if(status == "uploading") {
                        this.props.listUploadingStart(this.props.groupId, listId)
                    } else if(status == "done") {
                        this.props.listUploadingDone(this.props.groupId, listId, response.data.count)
                    }
                },
                showUploadList: false
            }
            return (
                <Upload {...uploadSettings}>
                    <Button disabled={item.file_uploading}>
                        <Icon type="upload" spin={item.file_uploading}/> Загрузить пользователей
                    </Button>
                </Upload> 
            )
    }
/*
    renderDeleteFile(item) {
        let handleOk = () => {
            this.setState({showDelete: false}, () => {
                this.props.deleteFile(this.props.groupId, item.list_id)
            })
        }
       return ( <Modal
                    title="Удаление списка пользователей"
                    visible={this.state.showDelete}
                    onOk={handleOk}
                    onCancel={() => this.setState({showDelete: false})}
                ><p>Вы точно хотите удалить файл со списком пользователей <b> {item.name}</b></p></Modal>
       )
    }
*/


    showConfirmDeleteUsers(delFunc, name) {
        confirm({
            title: "Удаление файла списка пользователей",
            content: <p>Вы точно хотите удалить файл со списком пользователей <b> {name}</b></p>,
            onOk() {
                delFunc()
            },
            onCancel() {},
        });
    }

    showConfirmDeleteList(delFunc, name) {
        confirm({
            title: "Удаление списка пользователей",
            content: <p>Вы точно хотите НАВСЕГДА удалить список <b> {name}</b>?</p>,
            onOk() {
                delFunc()
            },
            onCancel() {},
        });
    }


    render() {
        if (this.props.error) {
            let retryClb = () => {
                this.props.loadList(this.props.groupId)
            }
            return <ErrorView retryClb={retryClb} />
        }
        let list = this.props.list.map((adv) => {
            return {
                name: adv.name,
                description: adv.description,
                count: adv.users_uploaded_count,
                id: adv.id,
                deleting: adv.deleting,
                file_uploaded: adv.file_uploaded,
                file_uploading: adv.file_uploading
            }
        })
        if (this.props.loading) {
            return <Spin size="large"/>
        }
        let actions = (item) => {
            let act = []

            if(item.deleting) {
                act.push(<p>Удаление списка...</p>)
                return act
            }
            act.push(<a onClick={() => {
                let deleteFunc = () => {
                    this.props.deleteList(this.props.groupId, item.id)
                   console.log(deleteFunc, item.name)
                }
                this.showConfirmDeleteList(deleteFunc, item.name)
            }}>Удалить список</a>)

            if(item.file_uploaded) {
                act.unshift(<a onClick={() => {
                    let deleteFunc = () => {
                        this.props.deleteFile(this.props.groupId, item.id)
                    }
                    this.showConfirmDeleteUsers(deleteFunc, item.name)
                }}>Удалить пользователей</a>)
            } else {
                act.unshift(this.renderUploadFile(item))
            }
            return act
        }
        return (
            <span>
                <Button type="primary" icon="reload" size={'small'} onClick={() => { this.setState({showCreate: true})}}>Создать</Button> <br />
                {this.createDialog()}
                <List
                    className="demo-loadmore-list"
                    loading={false}
                    itemLayout="horizontal"
                    dataSource={list}
                    renderItem={item => (
                        <List.Item actions={actions(item)}>
                            <List.Item.Meta
                                avatar={<Avatar src="http://icons.iconarchive.com/icons/icons8/windows-8/256/Data-List-icon.png" />}
                                title={item.name}
                                description={item.description}
                            />
                            <div>Пользователи: {item.count}</div>
                        </List.Item>
            )}
          />
          </span>
        )
    }
}




const mapStateToProps = (state, ownProps) => {
    console.log({"adListState": state.advListMgmt.listAdvLists})
    let adv = state.advListMgmt.listAdvLists[ownProps.groupId]
    return {
        loading: adv ? adv.loading : undefined,
        list: adv && adv.list ? adv.list : [],
        create_loading: adv ? adv.create_loading : undefined,
        error: adv ? adv.error : false
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadList: (group_id) => dispatch(fetchAdvLists(group_id)),
        addList: (name, description, group_id) => dispatch(addAdvList(name, description, group_id)),
        listUploadingStart: (group_id, list_id) => dispatch(advUploadingStart(group_id, list_id)),
        listUploadingDone: (groupId, listId, count) => dispatch(advUploadingDone(groupId, listId, count)),
        deleteFile: (group_id, list_id) => dispatch(deleteAdvListUploaded(group_id, list_id)),
        deleteList: (group_id, list_id) => dispatch(deleteAdvList(group_id, list_id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdvListView);