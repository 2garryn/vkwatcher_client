import React from 'react';

import { fetchAlyticsCounters, resetAlyticsCounters } from './actions/alyticsMgmt';
import { connect } from 'react-redux';
import { Table, Spin } from 'antd';
import { eventTypeToTitle } from './utils.js';
import moment from 'moment'

class AlyticsViewTable extends React.Component {
    state = {
    }
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        this.props.loadCounters()
    }
    componentWillUnmount() {
        this.props.resetCounters()
    }

    tableColumns() {
        return [{
            title: 'Список пользователей',
            dataIndex: 'title',
            width: '15%'
      //      key: 'listName',
        }, {
            title: 'Описание',
            dataIndex: 'description'
        }, {
            title: "Дата загрузки списка",
            dataIndex: "created_on",
        },  {
            title: 'Количество',
            dataIndex: 'count',
           // sortOrder: 'descend',
            defaultSortOrder: 'descend',
            sorter: (a, b) => { return b - a },
           // sortOrder: 'ascend'
        //    key: 'count'
        }, {
            title: "Кол-во пользователей в списке",
            dataIndex: "users_uploaded_count"
        }];
    }


    getRawData() {
        return this.props.counters
            .map((co) => {
                return {
                    key: co.external_id,
                    title: co.name,
                    description: co.description,
                    created_on: moment.unix(co.created_on).format("HH:mm DD/MM/YYYY"),
                    users_uploaded_count: co.users_uploaded_count,
                    count: co.counters.reduce((acc, to) => { return acc + to.count }, 0),
                    children: co.counters.map((to) => {
                        return {
                            key: co.external_id + to.event_type,
                            title: eventTypeToTitle(to.event_type),
                            description: false,
                            count: to.count
                        }
                    })

                }
            })
    }

    render() {
        if (this.props.loading) {
            return <Spin size="large" /> 
        }
        return (
            <div> <Table columns={this.tableColumns()}  dataSource={this.getRawData()} /> </div>
        )
    }
}
const mapStateToProps = (state, props) => {
    let alytics = state.alyticsMgmt
    return {
        loading: alytics.counters_loading,
        counters: alytics.counters_list || []
    };
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        loadCounters: () => dispatch(fetchAlyticsCounters(props.startDate, props.endDate, props.groupId)),
        resetCounters: () => dispatch(resetAlyticsCounters()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AlyticsViewTable);