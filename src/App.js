import React, { Component } from 'react';
import Main from './Main'


class App extends Component {
  render() {
    return (
      <div style={{'height': '100vh'}}>
        <Main />
      </div>
    );
  }
}

export default App;
