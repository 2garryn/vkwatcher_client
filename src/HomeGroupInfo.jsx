import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';
import { Col, Row, Divider, Spin, Tabs } from 'antd';
import Dotdotdot from 'react-dotdotdot';
import './css/homeMainArea.css'
import EventsView from './EventsView';
import AdvListView from './AdvListView';
import AlyticsView from './AlyticsView';
import _ from 'underscore';
import { MuiThemeProvider } from '@material-ui/core';

const TabPane = Tabs.TabPane;

class KeyValueDescription extends React.Component {
    render() {
       return (
        <tr>
            <td style={{width:"50%", verticalAlign: "top"}}>
                <span className="kv-title">{this.props.title}</span>
            </td>
            <td style={{width:"50%", verticalAlign: "top"}}>
                <span className="kv-value">
                    {this.props.value}
                </span>
            </td>
        </tr>
        )

    }

}


class HomeGroupInfo extends React.Component {
    componentDidMount() {
        this.loadGroup()
    }
/*
    getSnapshotBeforeUpdate(prevProps, prevState) { 
        if(prevProps.id != this.props.id) {
            this.loadGroup()
        }
        return null
    }
*/
    loadGroup() {
        let id = this.props.id
        let grs = _.where(this.props.listGroups, {id: id})
        this.setState({selectedGroup: grs[0]})
    }


    groupInfo() {
        let sg = this.state.selectedGroup
        console.log({"SGGG": sg})
        return (
            <div>
                <Row>

                    <Col span={8}>
                        <table style={{width:"100%"}}> 
                            <tbody>
                            <KeyValueDescription title={"ID группы"} value={sg.id}/>
                            <KeyValueDescription title={"Название"} value={sg.name}/>
                            <KeyValueDescription title={"Код подтверждения"} value={sg.confirm_code}/>
                            <KeyValueDescription title={"Секретный ключ"} value={sg.secret_key}/>
                            <KeyValueDescription title={"Подтверждена"} value={sg.confirmed ? "Да" : "Нет"}/>
                            </tbody>
                        </table>
                    </Col>
                    <Col span={12}>   
                        <table style={{width:"100%"}}>  
                        <tbody>
                            <KeyValueDescription title={"Короткий адрес ВК"} value={sg.screen_name}/> 
                            <KeyValueDescription title={"Название ВК"} value={sg.vk_group_name}/>    
                            <KeyValueDescription title={"Описание"} value={<Dotdotdot clamp={2}>{sg.vk_group_description}</Dotdotdot>}/>   
                            <KeyValueDescription title={"Количество участников"} value={sg.vk_members_count}/> 
                            <KeyValueDescription title={"URL"} value={<a href={sg.vk_url} target="_blank">{sg.vk_url}</a>}/> 
                        </tbody>
                        </table>
                    </Col>
                    <Col span={4}>  
                        <img src={sg.vk_photo} />
                    </Col>
                </Row>
            </div>
        )

    }


    render() {
        if (this.state == null) {
            return <Spin size="large" />
        }
        console.log({"MY GROUPD PROPS": this.props})
        return (
            <div className={"mainDiv"}> 
                <div className={"title"}>
                {this.state.selectedGroup.name}
                </div>
                <Divider style={{height: '1px'}}/>
                <Tabs type="line" size="large" animated={false} tabPosition="top">
                    <TabPane tab="О группе" key="info">{this.groupInfo()}</TabPane>
                    <TabPane tab="События" key="events"><EventsView groupId={this.props.id} /></TabPane>
                    <TabPane tab="Списки" key="advLists"><AdvListView groupId={this.props.id} /></TabPane>
                    <TabPane tab="Аналитика по спискам" key="alyticsView"><AlyticsView groupId={this.props.id} /></TabPane>
                </Tabs>
            </div>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        listGroups: state.groupsMgmt.listGroups
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(HomeGroupInfo);