import React from 'react';
import { addGroup } from './actions/groupsMgmt'
import { connect } from 'react-redux';
import 'antd/dist/antd.css';
import { Form, Select, Input, Button, Spin } from 'antd';
const FormItem = Form.Item;

class HomeGroupAdd extends React.Component {

    state = {
        id: '',
        confirm_code: '',
        name: ''
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    addGroup() {
        let splitted = this.state.id.split('/')
        let gid = this.state.id
        if (splitted.length != 0) {
            gid = splitted[splitted.length - 1]
        }
        this.props.add_group(this.state.name, gid, this.state.confirm_code)
    }

    render() {
        if (this.props.add_group_loading) {
            return <Spin size="large" />
        }


        return (
            <Form onSubmit={this.handleSubmit}>
                 <FormItem
                    label="Название Группы"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 4 }}
                >
                    <Input onChange={this.handleChange('name')} />
                </FormItem>
                <FormItem
                    label="ID или URL группы"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 4 }}
                >
                    <Input onChange={this.handleChange('id')} />
                </FormItem>
                <FormItem
                    label="Код подтверждения"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 4 }}
                >
                    <Input onChange={this.handleChange('confirm_code')} />
                </FormItem>
                <FormItem wrapperCol={{ span: 4, offset: 5 }}>
                    <Button type="primary" htmlType="submit" disabled={this.state.id == '' || this.state.confirm_code == ''}onClick={() => {this.addGroup()}}>Добавить</Button>
                </FormItem>
            </Form>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        add_group_loading: state.groupsMgmt.addGroupLoading
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        add_group: (name, groupid, confirm_code) => dispatch(addGroup(name, groupid, confirm_code))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(HomeGroupAdd);