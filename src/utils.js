export function eventTypeToTitle(type) {
    switch(type) {
        case 'message_new': 
            return 'Новые сообщения'

        default: 
            return undefined
    }
}