import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { connect } from 'react-redux';

import {
    LineChart, 
    Line, 
    XAxis, 
    YAxis, 
    CartesianGrid, 
    Tooltip, 
    Legend,
    ResponsiveContainer} from 'recharts'
var ColorHash = require('color-hash');
//const {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} = recharts;
const data = [
      {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
      {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
      {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
      {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
      {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
      {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
      {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];

class AlyticsViewGraph extends React.Component {

    data() {
        return [
            {"local_day":"2018-9-5","count":0},
            {"local_day":"2018-9-6","count":50},
            {"local_day":"2018-9-7","count":0},
            {"local_day":"2018-9-8","count":0},
            {"local_day":"2018-9-9","count":0},
            {"local_day":"2018-9-10","count":30},
            {"local_day":"2018-9-11","count":45},
            {"local_day":"2018-9-12","count":25},
            {"local_day":"2018-9-13","count":123},
            {"local_day":"2018-9-14","count":67},
            {"local_day":"2018-9-15","count":0}
        ]

    }


    getData() {
        let allListEvents = this.props.events || {}
        let selectedLists = this.props.selectedLists
        let selectedEvents = this.props.selectedEvents 
        console.log({
            allListEvents,
            selectedLists,
            selectedEvents
        })

        let reduced = {}
        selectedLists.forEach(sl => {
            let ns = allListEvents[sl] || []
            ns.forEach(ae => {
                if(reduced[ae.local_day]) {
                    if(reduced[ae.local_day][sl]) {
                        reduced[ae.local_day][sl][ae.event_type] = ae.count
                    } else {
                        let n = {}
                        n[ae.event_type] = ae.count
                        reduced[ae.local_day][sl] = n
                    }
                } else {
                    let s = {}
                    s[ae.event_type] = ae.count
                    let n = {}
                    n[sl] = s
                    reduced[ae.local_day] = n
                }

            }) 
        });
        let redArray = []
        for (let key in reduced) {
            let l = reduced[key]
            l['local_day'] = key
            redArray.push(l)
        }  
        redArray = redArray.map(r => {
            let newR = {}
            for (let key in r) {
                if (key == 'local_day') {
                    newR['local_day'] = r[key]
                } else {
                    newR[key] = selectedEvents.reduce((currentValue, se) => {
                        if (se == 'all') {
                            for( let en in r[key]) {
                                currentValue += r[key][en]
                            }
                            return currentValue
                        } else {
                            currentValue += r[key][se]
                            return currentValue
                        }
                    }, 0)
                }
            }
            return newR
        })

        redArray = redArray.sort((a, b) => {
            let autc = moment(a.local_day).utc().valueOf()
            let butc = moment(b.local_day).utc().valueOf()
            return autc - butc
        })


        console.log({"REDUCED": redArray})

        return redArray
    }


    formatXAxis(date) {
        return moment(date).format('YYYY-MM-DD')
    }

    getName(listId) {
        return listId
    }

    render() {
        

        let lines = this.props.selectedLists.map(m => {
         //   let colorHash = ColorHash(;

            return <Line type="monotone" name={this.getName(m)} dataKey={m} stroke={(new ColorHash()).hex(m)} activeDot={{r: 2}}/>
        })

        return (
            <ResponsiveContainer width='100%' aspect={2.5/0.5}>
            <LineChart width={1000} height={500} data={this.getData()}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <XAxis 
                dataKey="local_day"
                name="Пользователи"
                tickFormatter={this.formatXAxis}
            />
           <YAxis/>
           <CartesianGrid strokeDasharray="3 3"/>
           <Tooltip/>
           <Legend />
            {lines}
          {/*} <Line type="monotone" dataKey="uv" stroke="#82ca9d" /> */}
          </LineChart>
          </ResponsiveContainer>
        );
    }
}

const mapStateToProps = (state, props) => {
    let data = state.alyticsMgmt[props.groupId]
    console.log({"DATA US HERE": data})
    return {
        events: data ? data.events : [],
        loading: data ? data.loading : true
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlyticsViewGraph);