import React from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import { connect } from 'react-redux';
import { fetchListGroups } from './actions/groupsMgmt'
import HomeAddGroup from './HomeAddGroup'
import HomeGroupInfo from './HomeGroupInfo'
import 'antd/dist/antd.css';
import { Layout, Menu, Icon, Spin } from 'antd';
import  Spinner  from './libs/Spinner'
const { Header, Footer, Sider, Content } = Layout;
const { SubMenu } = Menu;

const createGroupLabel = "Создать группу"


class LeftPanel extends React.Component {
    getItemTemplate(item) {
        return (
            <Menu.Item key={item.key} onClick={item.clb}>
                <span>{item.label}</span>
            </Menu.Item>
        )
    }
    render() {
        const { items, selectedKey } = this.props;
        return (
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['0']} selectedKeys={[selectedKey]}>
                {items.map((i) => {
                    return <SubMenu key={i.key} title={i.title}>
                        {i.list.map((g) => {
                            return this.getItemTemplate(g)
                        })}
                    </SubMenu>
                })}
            </Menu>
        )
    }
}

class Home extends React.Component {

    state = {
        selectedKey: ''
    }
    componentDidMount() {
        this.props.fetchListGroups()
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        let newGr = _.difference(this.props.listGroups, prevProps.listGroups)
        if (newGr.length > 0) {
            this.setState({selectedKey: 'groups' + newGr[0].id, keyType: 'groups', selectedId: newGr[0].id})
            return null
        }
        return null
    }
    getMenuItems() {
        const {listGroups, selectGroup} = this.props
        return [
            {
                key: 'groups',
                title: <span><Icon type="user" />Группы</span>,
                list: this.getItems(listGroups, createGroupLabel, 'groups', 'addGroup')
            }
        ]
    }


    getItems(items, createLabel, keyType, addKey) {
        let covertedItems = items.map((i) => {
            let key = keyType + i.id
            return {label: i.name, key: key, clb: () => {
                this.setState({selectedKey: key, keyType: keyType, selectedId: i.id})
            }}
        })
        covertedItems.unshift({
            label: createLabel, 
            key: addKey,
            clb: () => {
                this.setState({selectedKey: addKey, keyType: keyType, selectedId: addKey})
            }
        })
        return covertedItems
    }

    renderScreen() {
        return (
            <div>
                <Layout style={{height:"100vh"}}>
                    <Sider style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0}}>
                        <LeftPanel 
                            items={this.getMenuItems()}
                            selectedKey={this.state.selectedKey}/>
                    </Sider>
                    <Layout style={{height:"100vh", marginLeft: 200}}>
                        <Content>{this.renderContent()}</Content>
                    </Layout>
                </Layout>
            </div>    
        )
    }
    renderContent() {
        console.log({
            "STATE": this.state 
        })
        if (this.state.keyType == 'groups') {
            if(this.state.selectedId == 'addGroup') {
                return <HomeAddGroup />
            } else if(this.state.selectedId) {
                return <HomeGroupInfo key={this.state.selectedKey}  id={this.state.selectedId}/>
            } 
        } 
    }
    render() {
        if(this.props.listGroupsLoading) {
            return  <div> <Spinner size="large" /> </div>
        } else {
            return this.renderScreen()
        }
    }
}

const mapStateToProps = (state) => {
    return {
        listGroupsLoading: state.groupsMgmt.listGroupsLoading,
        listGroups: state.groupsMgmt.listGroups
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchListGroups: () => dispatch(fetchListGroups()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);