import React from 'react';
import { listEvents } from './actions/groupsMgmt'
import { connect } from 'react-redux';
import { Button, Table, Radio } from 'antd';
import 'antd/dist/antd.css';
import moment from 'moment'
import ErrorView from './ErrorView';
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const defaultRange = '24h'


class EventsNewMessageNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentRange: defaultRange,
            currentPage: 1
        }
      }
    
    componentDidMount() {
        this.props.list_events(this.props.groupId, defaultRange, 45, 0, true)
    }
/*
    getSnapshotBeforeUpdate(prevProps, prevState) { 
        if(prevProps.groupId != this.props.groupId) {
            this.props.list_events(this.props.groupId, defaultRange, 45, 0, true)
        }
        return null
    }
*/
    getColumns() {
        return [{
            title: 'Актор',
            dataIndex: 'user_id',
    //        key: 'user_id',
        }, {
            title: 'Дата',
            dataIndex: 'timestamp',
    //        key: 'timestamp',
        }]
    }

    getVkLink(user_id) {
        if(user_id) {
            let link = 'https://vk.com/id' + user_id.toString()
            return <a href={link} target="_blank">{link}</a>
        } else {
            return undefined
        }
    }

    getDataValues() {
        return this.props.events.map((el) => {
            return {
                user_id: this.getVkLink(el.user_id),
                timestamp: el.timestamp ? moment.unix(el.timestamp).format("HH:mm DD/MM/YYYY") : '',
                key: el.key
            }
        })
    }

    onChangeTimeRange(range) {
        this.setState({currentRange: range, currentPage: 1}, () => {
            this.props.list_events(this.props.groupId, range, 45, 0, true)
        })
    }

    onChangePage(page, pageSize) {
        this.setState({currentPage: page}, () => {
            let startItem = (page - 1) * 15
            if(this.props.events[startItem] && !this.props.events[startItem].received) {
                let limit = 0
                for(let i = startItem; i < this.props.events.length; i++) {
                    if(!this.props.events[i].received) {
                        limit++
                    } else {
                        break
                    }
                }
                if (limit > 45) {
                    limit = 45
                }
                this.props.list_events(this.props.groupId, defaultRange, limit, startItem, false)
            }
        })
    }

    render() {

        if (this.props.error) {
            let retryClb = () => {
                this.props.list_events(this.props.groupId, defaultRange, 45, 0, true)
            }
            return <ErrorView retryClb={retryClb} />
        }

        let pagination = {
            position: 'both', 
            hideOnSinglePage: false,
            total: this.props.count,
            defaultPageSize: 15,
            current: this.state.currentPage,
            onChange: (page, pageSize) => this.onChangePage(page, pageSize), 
        }
        let rowKey = (item) => { 
            return item.key.toString() 
        }
        return (
            <div>
                <RadioGroup onChange={(e) => { this.onChangeTimeRange(e.target.value) }} defaultValue={defaultRange}>
                    <RadioButton value="24h">24 часа</RadioButton>
                    <RadioButton value="3d">3 дня</RadioButton>
                    <RadioButton value="1w">Неделя</RadioButton>
                    <RadioButton value="2w">Две недели</RadioButton>
                    <RadioButton value="1m">Месяц</RadioButton>
                    <RadioButton value="3m">Три месяца</RadioButton>
                    <RadioButton value="6m">Полгода</RadioButton>
                </RadioGroup> <br />< br/>
                <Button type="primary" icon="reload" size={'small'} onClick={() => {
                    this.onChangeTimeRange(this.state.currentRange)
                }}>Обновить</Button> <br />
                {this.props.count != -1 ? "Всего " + this.props.count.toString() : ''}
                <Table pagination={pagination} rowKey={rowKey} loading={this.props.loading} dataSource={this.getDataValues()} columns={this.getColumns()} />
            </div>
        )
    }

}


const mapStateToProps = (state) => {
    let message_new = state.groupsMgmt.events['message_new']
    return {
        events: message_new && message_new.list ? message_new.list : [],
        loading: message_new && message_new.loading ? message_new.loading : false,
        count: message_new && message_new.count ? message_new.count : -1,
        error: message_new && message_new.error ? message_new.error : false
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        list_events: (groupId, definedTs, limit, offset, reload) => dispatch(listEvents(groupId, 'message_new', definedTs, limit, offset, reload))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(EventsNewMessageNew);