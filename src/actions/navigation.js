export function setMainScreen(label, page) {
    return {
        type: 'SET_MAIN_SCREEN',
        label,
        page
    };
}
