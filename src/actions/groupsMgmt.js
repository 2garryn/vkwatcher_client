import Req from '../data/req.js';


export function addGroupLoading(bool) {
    return {
        type: 'ADD_GROUP_LOADING',
        bool
    };
}


export function addGroupSuccess(group) {
    return {
        type: 'ADD_GROUP_SUCCESS',
        group
    };
}


export function addGroup(name, groupid, confirm_code)  {
    return (dispatch) => {
        dispatch(addGroupLoading(true));
        Req.addGroup(name, groupid, confirm_code)
        .then((r) => {
            console.log({"RETURNEG ROUP": r})
            if(r.ok) {
                dispatch(addGroupSuccess(r.data))
                dispatch(addGroupLoading(false));
            }
        })
    };
}


export function fetchListGroupsLoading(bool) {
    return {
        type: 'FETCH_LIST_GROUPS_LOADING',
        bool
    };
}

function fetchListGroupsSuccess(groups) {
    return {
        type: 'FETCH_LIST_GROUPS_SUCCESS',
        groups
    }
}


export function fetchListGroups() {
    return (dispatch) => {
        dispatch(fetchListGroupsLoading(true));
        Req.listGroups()
        .then((r) => {
            console.log({"Received list": r})
            if(r.ok) {
                dispatch(fetchListGroupsSuccess(r.data))
                dispatch(fetchListGroupsLoading(false));
            }
        })
    }
}


export function listEventsLoading(bool, eventType, reload) {
    return {
        type: 'LIST_EVENTS_LOADING',
        eventType,
        bool,
        reload
    }
}

export function listEventsSuccess(eventType, events, limit, offset) {
    return {
        type: 'LIST_EVENTS_SUCCESS',
        eventType,
        events,
        limit,
        offset
    }
}

export function listEventsError(groupId, eventType) {
    return {
        type: 'LIST_EVENTS_ERROR',
        eventType,
        groupId
    }
}


export function listEventsReceivedReqId(eventType, request_id, count) {
    return {
        type: 'LIST_EVENTS_RECEIVED_REQ_ID',
        eventType,
        request_id,
        count
    }
}

export function listEvents(groupId, eventType, definedTs, limit, offset, reload) {
    return (dispatch, getState) => {
        dispatch(listEventsLoading(true, eventType, reload))
        let event = getState().groupsMgmt.events[eventType]
        if(reload || !event.request_id) {
            Req.getEventsRequestId(groupId, eventType, definedTs, "0", "0")
            .then((val) => {
                if(!val.ok) throw "error"
                return val.data
            })
            .then((data) => {
                if(data.count == 0) {
                    dispatch(listEventsLoading(false, eventType, false))
                    throw 'no_result'
                }
                dispatch(listEventsReceivedReqId(eventType, data.request_id, data.count))
                return Req.listEventsByRequestId(groupId, data.request_id, limit, offset)
            })
            .then((val) => {
                if(!val.ok) throw "error"
                return val.data
            })
            .then((data) => {
                dispatch(listEventsLoading(false, eventType, false))
                dispatch(listEventsSuccess(eventType, data, limit, offset))
            }).catch((res) => {
                if(res == "error") {
                    dispatch(listEventsError(groupId, eventType))
                }
            })
        } else {
            Req.listEventsByRequestId(groupId, event.request_id, limit, offset)
            .then((val) => {
                if(!val.ok) throw "error"
                return val.data
            })
            .then((data) => {
                dispatch(listEventsLoading(false, eventType, false))
                dispatch(listEventsSuccess(eventType, data, limit, offset))
            }).catch((res) => {
                if(res == "error") {
                    dispatch(listEventsLoading(false, eventType, false))
                    dispatch(listEventsError(groupId, eventType))
                }
            })
        }
    }
}
