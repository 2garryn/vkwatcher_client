import Req from '../data/req.js';
import { showInternalError } from './errorMgmt'

export function addAdvListLoading(group_id, bool) {
    return {
        type: 'ADD_ADVLIST_LOADING',
        bool,
        group_id
    };
}


export function addAdvListSuccess(group_id, data) {
    return {
        type: 'ADD_ADVLIST_SUCCESS',
        data,
        group_id
    };
}


export function addAdvList(name, description, group_id)  {
    return (dispatch) => {
        dispatch(addAdvListLoading(group_id, true));
        Req.addAdvList(name, description, group_id)
        .then((val) => {
            dispatch(addAdvListLoading(group_id, false));
            return val
        })
        .then((r) => {
            if(r.ok) {
                dispatch(addAdvListSuccess(group_id, r.data))
            }
        })
    };
}


export function fetchAdvListsLoading(group_id, bool) {
    return {
        type: 'FETCH_ADVLISTS_LOADING',
        bool,
        group_id
    };
}

function fetchAdvListsSuccess(group_id, lists) {
    return {
        type: 'FETCH_ADVLISTS_SUCCESS',
        lists,
        group_id
    }
}

function fetchAdvListsError(group_id) {
    return {
        type: 'FETCH_ADVLISTS_ERROR',
        group_id
    }
}


export function fetchAdvLists(group_id) {
    return (dispatch) => {
        dispatch(fetchAdvListsLoading(group_id, true));
        Req.listAdvLists(group_id)
        .then((val) => {
            dispatch(fetchAdvListsLoading(group_id, false));
            return val
        })
        .then((r) => {
            if(r.ok) {
                dispatch(fetchAdvListsSuccess(group_id, r.data))
            } else {
                dispatch(fetchAdvListsError(group_id))
            }
        })
    }
}

export function advUploadingStart(group_id, list_id) {
    console.log({"advUploadingStart": group_id, ":L": list_id})
    return {
        type: 'ADV_LIST_UPLOADING_START',
        group_id,
        list_id
    }
}

export function advUploadingDone(group_id, list_id, users_count) {
    return {
        type: 'ADV_LIST_UPLOADING_DONE',
        group_id,
        list_id,
        users_count
    }
}

export function deleteAdvListUploadedLoading(group_id, list_id) {
    return {
        type: 'ADV_LIST_DELETE_UPLOADED_LOADING',
        group_id,
        list_id
    }
}

export function deleteAdvListUploadedDone(group_id, list_id) {
    return {
        type: 'ADV_LIST_DELETE_UPLOADED_DONE',
        group_id,
        list_id
    }
}

export function deleteAdvListUploaded(group_id, list_id) {
    console.log({"GROUPD ISS": group_id, "LISTRI D": list_id})
    return (dispatch) => {
        dispatch(deleteAdvListUploadedLoading(group_id, list_id));
        Req.deleteAdvListUploaded(list_id)
        .then((r) => {
            if(r.ok) {
                dispatch(deleteAdvListUploadedDone(group_id, list_id))
            }
        })
    }
}

export function deleteAdvListLoading(group_id, list_id, loading) {
    return {
        type: 'ADV_LIST_DELETE_LOADING',
        group_id,
        list_id,
        loading
    };
}
export function deleteAdvListSuccess(group_id, list_id) {
    return {
        type: 'ADV_LIST_DELETE_SUCCESS',
        group_id,
        list_id
    };
}
export function deleteAdvList(group_id, list_id) {
    return (dispatch) => {
        dispatch(deleteAdvListLoading(group_id, list_id));
        Req.deleteAdvList(list_id)
        .then((r) => {
            if(r.ok) {
                dispatch(deleteAdvListSuccess(group_id, list_id))
            }
        })
    }
}