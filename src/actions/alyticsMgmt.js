import Req from '../data/req.js';

export function fetchAlyticsLoading(group_id, bool) {
    return {
        type: 'FETCH_ALYTICS_LOADING',
        group_id,
        bool
    }
}

export function fetchAlyticsSuccess(start_date, end_date, group_id, list_id, data) {
    return {
        type: 'FETCH_ALYTICS_SUCCESS',
        group_id,
        list_id,
        start_date,
        end_date,
        data
    }
}


export function fetchAlytics(startDate, endDate, groupId) {
    return (dispatch) => {
        dispatch(fetchAlyticsLoading(groupId, true));
        Req.fetchAlytics(startDate, endDate, groupId, 'all')
        .then((r) => {
            if(r.ok) {
                dispatch(fetchAlyticsSuccess(startDate, endDate, groupId, r.data))
                dispatch(fetchAlyticsLoading(groupId, false));
            }
        })
    }
}

export function fetchAlyticsCountersLoading(groupId, bool) {
    return {
        type: 'FETCH_ALYTICS_COUNTERS_LOADING',
        groupId,
        bool
    }
}

export function fetchAlyticsCountersSuccess(startDate, endDate, groupId, countersList) {
    return {
        type: 'FETCH_ALYTICS_COUNTERS_SUCCESS',
        startDate,
        endDate,
        groupId,
        countersList
    }
}

export function resetAlyticsCounters() {
    return {
        type: 'RESET_ALYTICS_COUNTERS'
    }
}

export function fetchAlyticsCounters(startDate, endDate, groupId) {
    return (dispatch) => {
        dispatch(fetchAlyticsCountersLoading(groupId, true));
        Req.fetchAlyticsCounters(startDate, endDate, groupId)
        .then((r) => {
            if(r.ok) {
                dispatch(fetchAlyticsCountersSuccess(startDate, endDate, groupId, r.data))
                dispatch(fetchAlyticsCountersLoading(groupId, false));
            }
        })
    }
}