

export function showInternalError(retryClb, cancelClb) {
    return (dispatch) => {
        dispatch(showInternalErrorDispatched(dispatch, retryClb, cancelClb));
    }
}

function showInternalErrorDispatched(dispatch, retryClb, cancelClb) {
    return {
        type: 'SHOW_INTERNAL_ERROR',
        retryCallback: () => {
            dispatch(hideError())
            if(retryClb) {
                retryClb()
            }
        },
        cancelCallback: () => {
            dispatch(hideError())
            if(cancelClb) {
                cancelClb()
            }
        },
    }
}

function hideError() {
    return {
        type: 'HIDE_ERROR'
    }
}