import React from 'react';
import { Spin } from 'antd';
import '../css/spinner.css'

class Spinner extends React.Component {

    render() {
        return (
            <div className='spinner'>
                <Spin size={this.props.size} />
            </div>
        )
    }

}
export default Spinner