import * as Request from 'request';

class Req {

    url(m) {
        return 'https://ogarochek.ru' + m
    }

    login(login, password) {
        return this.postReq('/login', {login, password}).then(({status, data, error}) => {
            if(status == "ok") {
                sessionStorage.setItem('status','loggedIn') 
                window.location.reload();
                return true
            } else {
                return error
            }

        })
    }

    postLoginReq(path, body) {
        return new Promise((resolve) => {
            let strBody = JSON.stringify(body)
            Request.post(this.url(path), {body: strBody}, (_err, httpResp, _body) => {
                if(httpResp.statusCode == 200) {
                    let parsed = JSON.parse(httpResp.body)
                    resolve(parsed)
                } else {
                    resolve({
                        status: "error",
                        error: {code: 1, message: "internal error"}
                    })
                }
            })
        })
    }

    postReq(path, body) {
        return new Promise((resolve) => {
            let strBody = JSON.stringify(body)
            Request.post(this.url(path), {body: strBody}, (_err, httpResp, _body) => {
                if(httpResp.statusCode == 200) {
                    let parsed = JSON.parse(httpResp.body)
                    if(parsed.status == "error" && parsed.error.code == 20001) {
                        window.location.reload();
                    } else {
                        resolve(parsed)
                    }
                } else {
                    resolve({
                        status: "error",
                        error: {code: 1, message: "internal error"}
                    })
                }
            })
        })
    }

    getReq(path,  params) {
        return new Promise((resolve) => {
            Request.get({url: this.url(path), qs: params}, (err, httpResp, body) => {
                if(httpResp.statusCode == 200) {
                    let parsed = JSON.parse(httpResp.body)
                    if(parsed.status == "error" && parsed.error.code == 20001) {
                        window.location.reload();
                    } else {
                        resolve(parsed)
                    }
                } else {
                    resolve({
                        status: "error",
                        error: {code: 1, message: "internal error"}
                    })
                }
            })
        })
    }

    isLoggedIn() {
        if(sessionStorage.getItem('status')) return true
        else false
    }

    addGroup(name, id, confirm_code) {
        let body = {name, id, confirm_code}
        return this.postReq('/groups/create', body).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }
    
    listGroups() {
        return this.postReq('/groups/list', {}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }

    getEventsRequestId(group_id, type, defined_ts, start_ts, end_ts) {
        return this.postReq('/groups/events/getid', {group_id, type, defined_ts, start_ts, end_ts}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }


    listEventsByRequestId(group_id, request_id, limit, offset) {
        return this.postReq('/groups/events/list', {group_id, request_id, limit, offset}).then(({status, data, error}) => {
            console.log({"DAAAADA D ADA": data})
            return {ok: status == 'ok', data, error}
        })

    }
    listAdvLists(group_id) {
        return this.postReq('/advlist/list', {group_id}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }
    addAdvList(name, description, group_id) {
        return this.postReq('/advlist/create', {name, description, group_id}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }

    isAdvListUploaded(id) {
        return this.getReq('/advlist/is_uploaded', {id}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })     
    }
    deleteAdvListUploaded(external_id) {
        return this.postReq('/advlist/delete_upload', {external_id}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })           
    }
    deleteAdvList(external_id) {
        return this.postReq('/advlist/delete', {external_id}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })           
    }
    fetchAlytics(startDate, endDate, groupId, eventTypes) {
        return this.getReq('/alytics/get_alytics', {
            'group_id': groupId,
            'start_date': startDate,
            'end_date': endDate,
            'types': eventTypes
        }).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })     
    }
    fetchAlyticsCounters(start_date, end_date, group_id) {
        return this.postReq('/alytics/counters', {start_date, end_date, group_id, event_types: "all", external_ids: "all"}).then(({status, data, error}) => {
            return {ok: status == 'ok', data, error}
        })
    }
}


export default Req = new Req()