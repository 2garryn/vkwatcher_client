const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const overrides =  require('./src/themeOverrides.js').overrides;

console.log(overrides)

module.exports = function override(config, env) {
    config = injectBabelPlugin(
     ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
      config,
    );
   config = rewireLess.withLoaderOptions({
     modifyVars: overrides,
     javascriptEnabled: true,
   })(config, env);
    return config;
  };